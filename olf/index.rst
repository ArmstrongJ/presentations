=====================================
Introducing the Open Watcom Compilers
=====================================

Jeffrey Armstrong

https://github.com/open-watcom

25 October 2014

Introduction
============

.. rst-class:: build

* C, C++, and Fortran 77 Compilers

* Runtime Libraries

* Assembler

* Make Utility

* Integrated Development Environment

* Text and Graphical Debuggers

History
=======

* Originally commercial
 
  * Popular during MS-DOS's heyday

* Released as "open-source" in 2003

.. note::

    * Explain license

History
=======

* 2003 - Sybase releases OW as open-source

* 2003 - Ensure OW builds itself

* 2005 - x86 Linux support added

* 2009 - Independent Windows API implemented

* 2012 - 2.0 fork begins

  * x64 support
  
  * Improved C99 support
  
  * Work towards complete STL

Compiler Capabilities
=====================

.. rst-class:: build

* x86 (and recently x64) Code Generators

  * 16-bit, 32-bit, and 64-bit
  
* Some limitations on C++ compiler

* Complete support for fixed-format Fortran

* Cross-compiling

Compiler Hosts/Targets
======================

.. rst-class:: build

* MS-DOS / Windows3.x / Win32
  
* Windows NT
 
* OS/2
  
* Netware :sup:`1`
  
* x86/x64 Linux
  
* RDOS :sup:`1`

:sup:`1` Target only

Compiler Architecture
=====================

* Language-specific front end

* Architecture-specific back end

* Single executables

  * No intermediate files 

C Runtime Libraries
===================

* Independent implementations

* C89 complete

* Partial C99

* Common "legacy" extensions

C++ Runtime Libraries
=====================

* C++98 mostly complete

* Independent STL implementation
  
  * About 50% complete

* Supports 16-bit compilers

Fortran Runtime Libraries
=========================

* Complete Fortran 77 intrinsic support

* Odd Open Watcom extensions

* No Cray pointer support

Make Utility
============

* Complete "make" utility

* File-based

* Works on all supported hosts

.. note::

    * Useful for DOS and Windows developers 
    
Integrated Dev Environment
==========================

* Cross-platform IDE

  * Supports Win3.x/9x, OS/2, WinNT
  
* Ugly but functional

* Some interest in GTK+ port, but...

.. note::

    * Mention port can't proceed because of a shortcoming
    
Debuggers
=========

* Text-mode on all supported hosts

* GUI on Win3.x/9x, OS/2, WinNT

* Supports DWARF, CodeView, and Watcom debugging symbols

Project Summary
===============

* Substantially complex

* Complete development toolchain

Using Open Watcom
=================

* Getting Open Watcom
  
  * https://github.com/open-watcom
  
  * http://tx0.org/1r (Sourceforge)
  
  * http://buildbot.approximatrix.com/

Using Open Watcom
=================

* Environment Variables:

.. code-block:: sh

    WATCOM=<install directory>
    INCLUDE=$WATCOM/lh
    PATH=$WATCOM/binl:$PATH


Performance
===========

Whetstone - C

.. figure:: _static/whetstone.svg

Performance - MDBNCH
====================

MDBNCH - Fortran 77

.. figure:: _static/mdbnch.svg

Performance - Building Python
=============================

Building the Python Interpreter

.. figure:: _static/python.svg

Performance - Building Python
=============================

Building the Python Interpreter Take 2

.. figure:: _static/python_no_sre.svg

What Needs Work
===============

.. rst-class:: build

* C++ compiler

  * Lazy instantiation 

* STL completion

* Windows API

* GNU/Linux shared libraries

* More testing

Contact
=======

* Email: **jeff@rainbow-100.com**

* Twitter: **@fortranjeff**

* Github & BitBucket: **ArmstrongJ**

* Web: **http://jeff.rainbow-100.com/**

  * Slides: **http://tx0.org/1t**

Open Watcom
===========

https://github.com/open-watcom

.. figure:: _static/ow-qr.png
   :width: 500px
   :align: center

